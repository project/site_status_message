<?php

/**
 * @file
 * Site Status Message Variables File.
 *
 * @author: Gideon Cresswell (DrupalGideon)
 *          <https://www.drupal.org/u/drupalgideon>
 */

/**
 * Implements hook_variable_group_info().
 */
function site_status_message_variable_group_info() {
  $groups['site_status_message'] = array(
    'title'       => t('Site Status Message'),
    'description' => t('Site Status Message'),
    'access'      => 'administer site status message',
    'path'        => array('admin/config/system/site-status-message'),
  );

  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function site_status_message_variable_info($options) {
  $variable['site_status_message_message'] = array(
    'title'       => t('Status message', array(), $options),
    'description' => t('A message to display at the top of each page.', array(), $options),
    'localize'    => TRUE,
    'group'       => 'site_status_message',
    'type'        => 'string',
  );
  $variable['site_status_message_showlink'] = array(
    'title'       => t('Read more page', array(), $options),
    'description' => t('Optional "Read More" link to provide the viewer with more information.', array(), $options),
    'localize'    => TRUE,
    'group'       => 'site_status_message',
    'type'        => 'boolean',
  );
  $variable['site_status_message_link'] = array(
    'title'             => t('More information page', array(), $options),
    'description'       => t('An optional link to show more information about the status message.', array(), $options),
    'localize'          => TRUE,
    'group'             => 'site_status_message',
    'type'              => 'drupal_path',
    'validate callback' => 'site_status_message_link_variable_validate',
  );
  $variable['site_status_message_readmore'] = array(
    'title'       => t('More information link', array(), $options),
    'description' => t('The text to display on the "More Information" link.', array(), $options),
    'localize'    => TRUE,
    'group'       => 'site_status_message',
    'type'        => 'string',
  );
  $variable['site_status_message_display_options'] = array(
    'title'    => t('Where on the site should the message be displayed', array(), $options),
    'localize' => TRUE,
    'group'    => 'site_status_message',
    'type'     => 'select',
    'options'  => _site_status_message_get_display_options(),
  );

  return $variable;
}

/**
 * After build callback for Variable module to validate external link.
 *
 * @param array $variable
 *   The variable to validate.
 *
 * @return null|string
 *   If the link path is invalid, the error text to display.
 */
function site_status_message_link_variable_validate(array $variable) {
  // Validate that the More Information page exists.
  $link = $variable['value'];

  if (!empty($link) && !drupal_valid_path(drupal_get_normal_path($link))) {
    return t('You must enter a valid internal path.');
  }
}
