<?php

/**
 * @file
 * Site Status Message admin page.
 *
 * @author: Gideon Cresswell (DrupalGideon)
 *          <https://www.drupal.org/u/drupalgideon>
 */

/**
 * Site Status Message settings configuration form.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 *
 * @return array
 *   System settings form.
 */
function site_status_message_settings(array $form, array &$form_state) {
  $form['site_status'] = array(
    '#type'                        => 'fieldset',
    '#title'                       => 'Site Status Message',
    'site_status_message_message'  => array(
      '#type'          => 'textfield',
      '#maxlength'     => 256,
      '#title'         => t('Status message'),
      '#default_value' => variable_get('site_status_message_message', NULL),
      '#description'   => t('A message to display at the top of each page.'),
      '#weight'        => 0,
    ),
    'site_status_message_showlink' => array(
      '#type'          => 'checkbox',
      '#title'         => t('Read more page'),
      '#default_value' => variable_get('site_status_message_showlink', ''),
      '#description'   => t('Optional "Read More" link to provide the viewer with more information.'),
      '#weight'        => 10,
    ),
    'site_status_message_link'     => array(
      '#type'             => 'textfield',
      '#size'             => 40,
      '#maxlength'        => 256,
      '#title'            => t('More information page'),
      '#default_value'    => variable_get('site_status_message_link', NULL) ? drupal_get_path_alias(variable_get('site_status_message_link', NULL)) : NULL,
      '#description'      => t('An optional internal link to show more information about the status message.'),
      '#field_prefix'     => url(NULL, array('absolute' => TRUE)),
      '#weight'           => 20,
      '#element_validate' => array('_site_status_message_link_validate'),
      '#states'           => array(
        'visible' => array(
          ':input[name="site_status_message_showlink"]' => array('checked' => TRUE),
        ),
      ),
    ),
    'site_status_message_readmore' => array(
      '#type'          => 'textfield',
      '#size'          => 40,
      '#maxlength'     => 128,
      '#title'         => t('More information link text'),
      '#default_value' => variable_get('site_status_message_readmore', 'Read more'),
      '#description'   => t('The text to display on the "More Information" link.'),
      '#weight'        => 30,
      '#states'        => array(
        'visible' => array(
          ':input[name="site_status_message_showlink"]' => array('checked' => TRUE),
        ),
      ),
    ),
  );

  // Add the token tree UI.
  if (module_exists('token')) {
    $form['site_status']['site_status_message_message']['#description'] .= ' ' . t('This field supports tokens.') . ' ' . theme('token_tree_link');
  }

  $form['display'] = array(
    '#type'                               => 'fieldset',
    '#title'                              => t('Display options'),
    'site_status_message_display_options' => array(
      '#type'          => 'radios',
      '#title'         => t('Where on the site should the message be displayed'),
      '#options'       => _site_status_message_get_display_options(),
      '#default_value' => variable_get('site_status_message_display_options', 'public'),
    ),
    '#weight'                             => 40,
  );

  return system_settings_form($form);
}

/**
 * Validation callback for the Site Status Message link field.
 *
 * Callback for #element_validate.
 *
 * @param array $element
 *   The element field being validated.
 * @param array $form_state
 *   The form_state array.
 */
function _site_status_message_link_validate(array $element, array &$form_state) {
  // Validate that the More Information page exists.
  $link = $form_state['values']['site_status_message_link'];

  if (!empty($link) && !drupal_valid_path(drupal_get_normal_path($link))) {
    form_error($element, t('You must enter a valid internal path.'));
  }
}
