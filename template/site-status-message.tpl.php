<?php

/**
 * @file
 * This template handles the output of the Site Status Message.
 *
 * Variables available:
 * - $classes: List of CSS classes.
 * - $message: The message to display (run through filter_xss).
 * - $link: An optional link to a page with more information.
 *
 * @ingroup themeable
 */
?>
<div id="site-status-message" class="<?php print $classes; ?>" role="alert">
  <strong><?php print $message ?></strong><?php if ($link): ?> <?php print $link ?>
 <?php endif; ?>
</div>
