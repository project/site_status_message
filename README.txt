
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Customisation
 * The Future


INTRODUCTION
------------

Current Maintainer: Gideon Cresswell (DrupalGideon)
<https://www.drupal.org/u/drupalgideon>

The Site Status Message is a simple module to display a site wide message to
your users at the top of each page. Use cases could be to inform of known
downtime in the future, to advertise a special offer on the site or some
important news that needs highlighting.

An optional link to a page with more information can be displayed after the
message.

It was completely inspired by the Drupal.org downtime message displayed October
2013 before the update of Drupal.org from Drupal 6 to Drupal 7.


REQUIREMENTS
------------

None.


INSTALLATION
------------

Install as usual, see
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.


CONFIGURATION
-------------

The module can be configured on the Site Status Message admin page
admin/config/system/site-status-message. The message to be displayed can be up
to 256 characters in length and the page with more information is an internal
path on the site.

The message can optionally be displayed on all the Admin pages of the site too.

If the message box is left blank, no message will be displayed.


CUSTOMISATION
-------------

The module ships with a template file that can be overridden in your own theme.

Copy the entire site-status-message.tpl.php into your theme directory to make
your own HTML changes.

Further customisation can be made by copying the preprocess function
site_status_message_preprocess_site_status_message() to your template.php and
rename to THEME_preprocess_site_status_message().

Two CSS selectors are provided which can be overridden in your own CSS. There
is the #site-status id and the .site-status-message class.


ADVANCED CUSTOMISATION
----------------------

The message is rendered in the "page_top" region on the page which is part of
the html.tpl.php template file. It is possible to move the message to another
region or block, e.g. inside page.tpl.php with code similar to below.

Inside your template.php file -

/**
 * Implements template_preprocess_html().
 */
function THEME_preprocess_html(&$variables) {
  if (isset($variables['page']['page_top']['site_status_message'])) {
    unset($variables['page']['page_top']['site_status_message']);
  }
}

Then inside the page.tpl.php you can call

<?php print render($page['page_top']['site_status_message']); ?>

to render the message.


MULTILINGUAL SUPPORT
--------------------

Requires the Variable module https://www.drupal.org/project/variable and
Internationalization module https://www.drupal.org/project/i18n

Added by SilviaT https://www.drupal.org/u/silviat


SUBMODULES
----------

The following submodules are currently included.

1) Site Status Message External Link

This module allows for external links to be used. Install this as usual and an
external link can be used for the more information page link.

Only valid external links can be used and the link will be checked every
cron run. If the link becomes invalid, the link is removed from the message.

Options are provided to allow the external link to open in the same browser
window or a new tab/window. There is also the option to add 'rel="nofollow"'
to the link which can prevent search engines from using the link to influence
results.

2) Site Status Message Scheduler

Requires Date Popup module (part of the Date module package)
https://www.drupal.org/project/date

3) Site Status Message Message Type

Allows for a the message to have a severity type and a different background
colour to be used depending on the severity. Out of the box, four types are
included: Information, Success, Warning and Danger.

These values can be altered via a hook implementation of
hook_site_status_message_message_types_alter().

Each message type adds a new css class which changes the background colour.


THE FUTURE
----------

In the future I would like to add these enhancements -

 * Show messages on specific site pages
 * Multiple messages which can be queued
