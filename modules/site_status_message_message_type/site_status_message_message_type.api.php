<?php

/**
 * @file
 * API documentation for Site Status Message Message Type module.
 *
 * @author: Gideon Cresswell (DrupalGideon)
 *          <https://www.drupal.org/u/drupalgideon>
 */

/**
 * Alter the list of message types.
 *
 * @param array $message_types
 *   Array of message types to alter.
 */
function hook_site_status_message_message_types_alter(array &$message_types) {
  // Add a new message type.
  $message_types[] = array(
    'name'  => t('Alert'),
    'class' => 'alert',
  );
}
