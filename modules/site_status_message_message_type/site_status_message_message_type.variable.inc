<?php

/**
 * @file
 * Site Status Message Message Type Variables File.
 *
 * @author: Gideon Cresswell (DrupalGideon)
 *          <https://www.drupal.org/u/drupalgideon>
 */

/**
 * Implements hook_variable_info().
 */
function site_status_message_message_type_variable_info($options) {
  $variable['site_status_message_message_type'] = array(
    'title'    => t('Specify the type of message this should be displayed as. Each type can have its own colour and styling.', array(), $options),
    'localize' => TRUE,
    'group'    => 'site_status_message',
    'type'     => 'select',
    'options'  => _site_status_message_get_message_types(),
  );

  return $variable;
}
