<?php

/**
 * @file
 * Site Status Message Scheduler Variables File.
 *
 * @author: Gideon Cresswell (DrupalGideon)
 *          <https://www.drupal.org/u/drupalgideon>
 */

/**
 * Implements hook_variable_info().
 */
function site_status_message_scheduler_variable_info($options) {
  $variable['site_status_message_schedule'] = array(
    'title'       => t('Schedule messages', array(), $options),
    'description' => t('Schedule the message to start in the future.', array(), $options),
    'localize'    => TRUE,
    'group'       => 'site_status_message',
    'type'        => 'boolean',
  );
  $variable['site_status_message_use_end_date'] = array(
    'title'    => t('Use end date', array(), $options),
    'localize' => TRUE,
    'group'    => 'site_status_message',
    'type'     => 'boolean',
  );
  $variable['site_status_message_start_date'] = array(
    'title'            => t('Start date', array(), $options),
    'description'      => t('Choose the date and time when the message will be displayed from.', array(), $options),
    'localize'         => TRUE,
    'group'            => 'site_status_message',
    'type'             => 'string',
    'element callback' => 'site_status_message_scheduler_variable_date',
  );
  $variable['site_status_message_end_date'] = array(
    'title'            => t('End date', array(), $options),
    'description'      => t('Choose the date and time when the message will stop being displayed.', array(), $options),
    'localize'         => TRUE,
    'group'            => 'site_status_message',
    'type'             => 'string',
    'element callback' => 'site_status_message_scheduler_variable_date',
  );

  return $variable;
}

/**
 * Callback for date variable element.
 */
function site_status_message_scheduler_variable_date($variable, $options) {
  $element = variable_form_element_default($variable, $options);
  $element['#type'] = 'date_popup';

  return $element;
}
