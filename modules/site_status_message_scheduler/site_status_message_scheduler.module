<?php

/**
 * @file
 * Site Status Message Scheduler.
 *
 * Allows for scheduling Site Status Message messages.
 *
 * @author: Gideon Cresswell (DrupalGideon)
 *          <https://www.drupal.org/u/drupalgideon>
 */

/* @noinspection PhpDocMissingThrowsInspection */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function site_status_message_scheduler_form_site_status_message_settings_alter(&$form, &$form_state, $form_id) {
  $form['scheduler'] = array(
    '#type'                            => 'fieldset',
    '#title'                           => t('Scheduler'),
    '#description'                     => t('This allows you to schedule messages to show on a future date and optionally to run for a certain period.'),
    '#weight'                          => 30,
    'site_status_message_schedule'     => array(
      '#type'          => 'checkbox',
      '#title'         => t('Schedule messages'),
      '#description'   => t('Schedule the message to start in the future.'),
      '#default_value' => variable_get('site_status_message_schedule'),
      '#weight'        => 5,
    ),
    'site_status_message_use_end_date' => array(
      '#type'          => 'checkbox',
      '#title'         => t('Use end date'),
      '#default_value' => variable_get('site_status_message_use_end_date'),
      '#weight'        => 10,
      '#states'        => array(
        'visible' => array(
          ':input[name="site_status_message_schedule"]' => array('checked' => TRUE),
        ),
      ),
    ),
    'site_status_message_start_date'   => array(
      '#type'             => 'date_popup',
      '#title'            => t('Start date'),
      '#description'      => t('Choose the date and time when the message will be displayed from.'),
      '#default_value'    => variable_get('site_status_message_start_date', date('Y-m-d H:i:s')),
      '#date_format'      => 'Y-m-d H:i:s',
      '#date_type'        => DATE_DATETIME,
      '#date_increment'   => 1,
      '#date_year_range'  => '0:+3',
      '#weight'           => 11,
      '#states'           => array(
        'visible' => array(
          ':input[name="site_status_message_schedule"]' => array('checked' => TRUE),
        ),
      ),
    ),
    'site_status_message_end_date'     => array(
      '#type'             => 'date_popup',
      '#title'            => t('End date'),
      '#description'      => t('Choose the date and time when the message will stop being displayed.'),
      '#default_value'    => variable_get('site_status_message_end_date', date('Y-m-d H:i:s')),
      '#date_format'      => 'Y-m-d H:i:s',
      '#date_type'        => DATE_DATETIME,
      '#date_increment'   => 1,
      '#date_year_range'  => '0:+3',
      '#weight'           => 12,
      '#states'           => array(
        'visible' => array(
          ':input[name="site_status_message_schedule"]'     => array('checked' => TRUE),
          ':input[name="site_status_message_use_end_date"]' => array('checked' => TRUE),
        ),
      ),
    ),
  );
  $form['#validate'][] = '_site_status_message_scheduler_date_validate';
}

/**
 * Validation callback for Scheduler fields.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 */
function _site_status_message_scheduler_date_validate(array &$form, array &$form_state) {
  $values = $form_state['values'];
  $schedule = $values['site_status_message_schedule'];
  $start_date = $values['site_status_message_start_date'];
  $use_end_date = $values['site_status_message_use_end_date'];
  $end_date = $values['site_status_message_end_date'];

  $now = new DateTime();
  $start = new DateTime($start_date);
  $end = new DateTime($end_date);

  if ($schedule && $use_end_date) {
    // Check the end date is not in the past.
    if ($end->getTimestamp() < $now->getTimestamp()) {
      form_error($form['scheduler']['site_status_message_end_date'], t('End date cannot be in the past.'));
    }

    // Check the end date is after the start date.
    if ($end->getTimestamp() < $start->getTimestamp()) {
      form_error($form['scheduler']['site_status_message_end_date'], t('End date cannot be before the start date.'));
    }
  }
}

/**
 * Implements hook_page_alter().
 */
function site_status_message_scheduler_page_alter(&$page) {
  // Check if the message is due to be scheduled.
  if (variable_get('site_status_message_schedule')) {
    $hide_message = FALSE;
    $start_date = variable_get('site_status_message_start_date', date('Y-m-d H:i:s'));
    $end_date = variable_get('site_status_message_use_end_date') ? variable_get('site_status_message_end_date', date('Y-m-d H:i:s')) : NULL;

    $now = new DateTime();
    $start = new DateTime($start_date);

    if ($start->getTimestamp() > $now->getTimestamp()) {
      // Hide the message if the start date/time is in the future.
      $hide_message = TRUE;
    }

    // Check if the scheduler uses an end date.
    if ($end_date) {
      $end = new DateTime($end_date);
      if ($end->getTimestamp() <= $now->getTimestamp()) {
        // Hide the message if the end date/time is in the past.
        $hide_message = TRUE;
      }
    }

    if ($hide_message) {
      // Remove the message from the page_top array so it won't be rendered.
      unset($page['page_top']['site_status_message']);
    }
  }
}
