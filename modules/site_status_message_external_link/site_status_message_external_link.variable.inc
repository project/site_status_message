<?php

/**
 * @file
 * Site Status Message External Link Variables File.
 *
 * @author: Gideon Cresswell (DrupalGideon)
 *          <https://www.drupal.org/u/drupalgideon>
 */

/**
 * Implements hook_variable_info().
 */
function site_status_message_external_link_variable_info($options) {
  $variable['site_status_message_use_external_link'] = array(
    'title'       => t('Use external link', array(), $options),
    'description' => t('Use external rather than internal link.', array(), $options),
    'localize'    => TRUE,
    'group'       => 'site_status_message',
    'type'        => 'boolean',
  );
  $variable['site_status_message_external_link'] = array(
    'title'       => t('More information page', array(), $options),
    'description' => t('An optional external link to show more information about the status message. Include http:// or https:// in the link.', array(), $options),
    'localize'    => TRUE,
    'group'       => 'site_status_message',
    'type'        => 'external_path',
  );
  $variable['site_status_message_external_link_new_tab'] = array(
    'title'    => t('Open link in a new browser tab.', array(), $options),
    'localize' => TRUE,
    'group'    => 'site_status_message',
    'type'     => 'boolean',
  );
  $variable['site_status_message_external_link_no_follow'] = array(
    'title'    => t("Use %nofollow attribute.", array('%nofollow' => 'rel="nofollow"'), $options),
    'localize' => TRUE,
    'group'    => 'site_status_message',
    'type'     => 'boolean',
  );

  return $variable;
}

/**
 * Implements hook_variable_type_info().
 */
function site_status_message_external_link_variable_type_info() {
  $type['external_path'] = array(
    'title'             => t('External path'),
    'element callback'  => 'site_status_message_external_path_element',
    'validate callback' => 'site_status_message_external_link_link_variable_validate',
    'localize'          => TRUE,
  );

  return $type;
}

/**
 * Callback for path variable element.
 *
 * @param array $variable
 *   The variable.
 * @param array $options
 *   Array of options.
 *
 * @return array
 *   The variable element.
 */
function site_status_message_external_path_element(array $variable, array $options) {
  $element = variable_form_element_default($variable, $options) + array(
    '#type' => 'textfield',
    '#size' => 40,
  );

  return $element;
}

/**
 * Validate callback for Variable module to validate external link.
 *
 * @param array $variable
 *   The variable to validate.
 *
 * @return null|string
 *   If the link path is invalid, the error text to display.
 */
function site_status_message_external_link_link_variable_validate(array $variable) {
  $link = $variable['value'];
  $valid_link = _site_status_message_external_link_is_url_valid($link);

  if (!$valid_link) {
    return t('You must enter a valid external path.');
  }

  variable_set('site_status_message_valid_external_link', $valid_link);
}
